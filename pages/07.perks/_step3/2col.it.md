---
title: 'Passo 3'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Passo 3:
##  Fai una donazione a Disroot

![](../../01.home/_donation/premium.png?class=reward) **Disroot dipende dal tuo contributo finanziario**. Questo è ciò che ci consente di essere completamente indipendenti, aiutandoci a pagare le bollette per l'hosting di **Disroot**.
---

<br>
**Il collegamento al dominio personalizzato è una funzionalità permanente**. Non c'è un abbonamento e nemmeno un cartellino del prezzo ad esso allegato. **Decidi tu quanto apprezzi il nostro lavoro e il nostro servizio** e quindi anche quanto sei disposto e in grado di donare per questa funzione. 

**Come indicatore proponiamo l'equivalente di 12 caffè all'anno** (invita gli amministratori di Disroot a prendere una tazza del tuo caffè preferito al tuo bar preferito😋). Quale caffè e a quale prezzo, **è una tua scelta**. Tieni presente che il benessere della piattaforma significa anche benessere della tua comunicazione.

Per donare a **Disroot**, scegli una delle seguenti opzioni. 
**Assicurati di inserire un nome/nickname di riferimento per la tua donazione**, qualcosa che puoi utilizzare nel modulo di richiesta sottostante in modo che possiamo verificare che tu abbia fatto una donazione. Una volta elaborata la richiesta, le informazioni di riferimento che ti collegano al tuo account verranno rimosse (attenzione, questo è quello che possiamo fare noi. Tuttavia, la banca o Paypal traccerà la transizione e su questo non abbiamo alcun controllo).

#### Donazioni:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Dona usando Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Diventa un Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

#### Coordinate bancarie:
<span style="color:#8EB726; font-size:1.6em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>

Carta di credito:<br><span style="color:#8EB726;"> Puoi utilizzare il pulsante di Paypal per le donazioni con carta di credito (non è necessario un account PayPal).  </span>
<br><br>
