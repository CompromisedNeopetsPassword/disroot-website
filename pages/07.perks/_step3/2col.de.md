---
title: 'Schritt 3'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Schritt 3:
##  Spende an Disroot

![](../../01.home/_donation/premium.png?class=reward) **Disroot ist abhängig von Deinem finanziellen Beitrag**. Das ermöglicht es uns, vollkommen unabhängig zu sein, und bezahlt die Rechnungen des Hostings von **Disroot**. Außerdem hilft es uns bei unserem ultimativen Ziel, Essen auf unseren Tisch zu bringen.


---

<br>
**Die Verknüpfung der benutzerdefinierten Domain ist ein lebenslanges Feature**. Es gibt kein Abonnement oder ein Preisschild daran. **Du entscheidest, wie Du unsere Arbeit und unseren Service bewertest** und wie viel Du bereit und in der Lage bist, für dieses Feature zu spenden.

**Als Indikator schlagen wir das Äquivalent von 12 Kaffee pro Jahr vor** (Deine Admins zu einer Tasse Deines Lieblingskaffees von Deinem bevorzugten Café sollte ein Menschenrecht sein 😋 ). Welcher Kaffee und welcher Preis **ist Deine Entscheidung**. Denk einfach daran, dass das Wohlergehen der Plattform auch das Wohlergehen Deiner Kommunikation bedeutet. Also lass sie uns gesund erhalten.

Um an **Disroot** zu spenden, wähle eine der folgenden Optionen.
**Stelle sicher, dass Du eine Referenz für Deine Spende erstellst**, etwas, das Du im Anfrageformular verwenden kannst, sodass wir erkennen können, dass Du gespendet hast. Sobald Deine Anfrage bearbeitet wurde, werden die auf Deinen Account verweisenden Referenzinformationen durch unsere Administration entfernt (allerdings werden sie das, aus offensichtlichen Gründen, nicht von Deiner Bank oder Deinem Paypal, da wir darauf keinen Einfluss haben, also behalte das im Hinterkopf).

#### Online-Spende:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Spende mit Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Werde ein Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

#### Banküberweisung:
<span style="color:#8EB726; font-size:1.6em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>

Kreditkarte:<br><span style="color:#8EB726;"> Du kannst den blauen Paypal-Button für Kreditkarten-Spenden nutzen (Paypal-Account wird nicht benötigt). </span>
<br><br>
