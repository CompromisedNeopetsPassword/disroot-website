---
title: 'Paso 3'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Paso 3:
##  Haz una donación a Disroot

![](../../01.home/_donation/premium.png?class=reward) **Disroot depende de tu contribución económica**. Eso es lo que nos permite ser completamente independientes y lo que paga las cuentas del mantenimiento de **Disroot**, así como también suma para el objetivo final de poner comida en nuestras mesas.


---

<br>
**La vinculación de dominios personalizados es una funcionalidad de por vida**. No hay suscripción ni tiene una etiqueta de precio pegada. **Tú decides cuánto valoras nuestro trabajo y nuestro servicio** y cuánto estás dispuestx y puedes donar por esta característica.

**Como un indicador, proponemos el equivalente a 12 tazas de café al año** (invitar una taza de tu café preferido a tus administradorxs es algo que debería ser un derecho humano 😋 ). Qué café y de qué precio, **es tu elección**. Solo ten presente que el bienestar de la plataforma es también el bienestar de tus comunicaciones. Así que mantengámosla saludable.

Para donar a **Disroot**, selecciona alguna de las siguientes opciones.
**Asegúrate de crear un nombre de referencia para tu donación**, algo que puedas utilizar en el formulario de solicitud a continuación, así podemos verificar que has donado. Una vez que la solicitud es procesada, la información de referencia vinculada a tu cuenta es eliminada de nuestra administración (sin embargo, por obvias razones, no lo será del registro bancario o de Paypal, porque no tenemos ningún control sobre eso, así que tenlo en mente).

#### Donación en línea:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Donar utilizando Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Conviértete en Mecenas" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="/cryptocurrency"><img alt="Criptomonedas" src="donate/_donate/c_button.png" /></a>

</div>

#### Transferencia bancaria:
<span style="color:#8EB726; font-size:1.6em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>

Tarjetas de crédito:<br><span style="color:#8EB726;"> Puedes utilizar el botón azul de Paypal para realizar donaciones con tarjeta de crédito (no es necesaria una cuenta de Paypal). </span>
<br><br>
