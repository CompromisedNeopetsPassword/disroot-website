---
title: 'Zusätzlicher Speicherplatz'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Zusätzlicher Speicherplatz für E&#8209;Mail, Cloud und/oder Cryptpad

![email](logo_email.png?resize=150) ![cloud](logo_cloud.png?resize=150)

---      

<br>
Mit Deinem **Disroot**-Account erhältst Du FREIEN Speicherplatz: 1GB für E&#8209;Mails, 2GB Cloudspeicherplatz, 500MB für Cryptpad. Es ist möglich diesen Speicherplatz zu erweitern.

Dies sind die Preise **pro Jahr, Zahlungsgebühren inklusive**:

||||
|---:|---|---:|
| 5GB |......| 11€ |
| 10GB |......| 20€ |
| 15GB |......| 29€ |
| 30GB |......| 56€ |
| 45GB |......| 83€ |
| 60GB |......| 110€ |


<br>
Auf Transaktionen innerhalb der EU wird eine zusätzliche VAT (Value Added Tax) von 21 % fällig.

Du kannst den zusätzlichen Speicherplatz nach Belieben zwischen E&#8209;Mail, Cloud und Cryptpad aufteilen. Wenn Du zum Beispiel 10 GB Speicherplatz bekommst, kannst Du 8 GB für die Cloud und 2 GB für E&#8209;Mails verwenden.

<a class="button button1" href="/forms/extra-storage-space">Zusätzlichen Speicherplatz beantragen</a>
