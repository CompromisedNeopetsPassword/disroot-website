---
title: 'Schritt 4'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
#bgcolor: '#C6FF9D'
#fontcolor: '#555'
wider_column: left
---

# Schritt 4:
## Beantrage die Verknüpfung mit deiner eigenen Domain


---
<br>
<br>

Fast am Ziel...
Wenn Du Deine Domain besitzt, DNS eingerichtet und Deinen Admins frischen Kaffee eingeflößt hast, kannst Du das Formular ausfüllen und Deine Anfrage zur Verknüpfung mit einer benutzerdefinierten Domain einreichen.

Wir bearbeiten die Anfragen jeden Freitagnachmittag. Wenn sie bearbeitet ist, erhältst Du eine E&#8209;Mail mit allen benötigten Informationen, um Dich mit deiner eigenen Domain an den Start zu bringen.
<br>
<a class="button button1" href="https://disroot.org/forms/domain-linking-form">Domain-Verknüpfung beantragen</a>
