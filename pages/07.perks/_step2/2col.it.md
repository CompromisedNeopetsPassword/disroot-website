---
title: 'Passo 2'
bgcolor: '#1F5C60'
fontcolor: '#fff'
wider_column: right
---

# Passo 2:
## Configura il DNS

Una volta che hai il tuo dominio, devi fare due cose per collegarlo a disroot.org.
*(I seguenti passaggi vengono eseguiti tramite l'interfaccia di aggiunta dei record DNS fornita dal tuo provider di domini)*:


---      

## 1: Dimostra di essere il proprietario del dominio
Crea semplicemente un record **TXT** con: **disroot.org-yourdomain.tld** (sostituendo `yourdomain.tld` con **il tuo nome di dominio**). Questo dimostrerà che possiedi quel dominio.

## 2: Punta il tuo dominio su disroot.org
**Primo** Imposta il record MX su **disroot.org.** **`(non dimenticare il punto finale!)`** 
**Secondo** Imposta **"v=spf1 mx a ptr include:disroot.org -all"** Questo ti permetterà di inviare email dai tuoi server così come da **Disroot**. Questa è solo un'impostazione predefinita che dovrebbe essere applicabile alla maggior parte degli scenari. Se vuoi saperne di più sui record SPF e desideri modificare i tuoi, controlla ad esempio [questa pagina](https://www.dmarcanalyzer.com/spf/how-to-create-an-spf-txt-record/) per maggiori informazioni.
