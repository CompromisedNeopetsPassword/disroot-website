---
title: Email
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">S'inscrire sur Disroot</a>
<a class="button button1" href="https://webmail.disroot.org/">Se connecter</a>
<a class="button button1" href="http://mdlwkwn2nhzzuymmyr5mjyoqppoyp46k4remaliu2zrmkayl5yfeh7ad.onion">Tor</a>

---

## E-mail

**Disroot** fournit des comptes de messagerie sécurisés pour votre client de bureau ou via une interface web.
La communication entre vous et le serveur de messagerie est chiffrée avec SSL, ce qui assure une confidentialité maximale. De plus, tous les courriels envoyés à partir de notre serveur sont également chiffrés (TLS) si les destinataires l'acceptent. Cela signifie que les courriels ne sont plus envoyés comme des "cartes postales" traditionnelles, mais sont mis dans une "enveloppe".

**Cependant, nous vous encourageons à toujours faire preuve de prudence lors de l'utilisation des communications par courriel et à utiliser le chiffrement GPG pour assurer la sécurité de votre correspondance.**
