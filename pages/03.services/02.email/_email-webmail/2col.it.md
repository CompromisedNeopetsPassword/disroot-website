---
title: 'Email Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>
Puoi accedere alla tua posta da qualsiasi dispositivo utilizzando la nostra webmail all'indirizzo [https://webmail.disroot.org](https://webmail.disroot.org)


La nostra webmail usa **Roundcube**. Roundcube è un software semplice, veloce e fornisce la maggior parte delle funzionalità che ci si può aspettare da una webmail:

<ul class=disc>
<li>Una bella interfaccia grafica reattiva: una bellissima skin dal sapore Disroot con supporto multi-dispositivo.</li>
<li>Traduzioni: l'interfaccia è tradotta in più di 80 lingue.</li>
<li>Gestione dei messaggi drag-and-drop: sposta i tuoi messaggi con il mouse.</li>
<li>Pieno supporto per messaggi MIME e HTML.</li>
<li>Anteprime allegati: guarda le immagini direttamente all'interno dei tuoi messaggi.</li>
<li>Elenco dei messaggi in thread: visualizza le conversazioni in modalità thread.</li>
<li>Filtri: imposta le regole per spostare o copiare automaticamente le e-mail in cartelle specifiche, inoltrare o rifiutare le e-mail, in base all'oggetto, al mittente, ecc.</li>
<li>Cartelle: gestisci le tue cartelle (aggiungi, rimuovi o nascondi).</li>
</ul>


Homepage del progetto: [https://roundcube.net](https://roundcube.net)
