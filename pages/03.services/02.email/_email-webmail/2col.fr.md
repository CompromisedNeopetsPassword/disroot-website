---
title: 'Email Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>
Vous pouvez accéder à votre courrier depuis n'importe quel appareil en utilisant notre webmail à l'adresse [https://webmail.disroot.org](https://webmail.disroot.org).

Notre webmail est alimenté par **Roundcube**. Roundcube est une approche moderne du webmail. C'est simple, rapide et fournit la plupart des fonctionnalités que l'on peut attendre d'un webmail :

<ul class=disc>
<li>Un joli theme réactif : un bel habillage à la saveur de Disroot avec une prise en charge multi-appareils.</li>
<li>Traductions : l'interface est traduite dans plus de 80 langues..</li>
<li>Gestion des messages par glisser-déposer : déplacez vos messages avec votre souris..</li>
<li>Prise en charge complète des messages MIME et HTML.</li>
<li>Aperçu des pièces jointes : visualisez les images directement dans vos messages..</li>
<li>Liste de messages en fil de discussion : Afficher les conversations en mode filaire.</li>
<li>Filtres : Définissez des règles pour déplacer ou copier automatiquement les e-mails dans des dossiers spécifiques, les transférer ou les rejeter, en fonction de l'objet, de l'expéditeur, etc.</li>
<li>Dossiers : Gérer vos dossiers (ajouter, supprimer ou masquer).</li>
</ul>


Project Homepage: [https://roundcube.net](https://roundcube.net)
