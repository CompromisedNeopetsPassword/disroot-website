---
title: 'Email Encryption'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## What is encryption

---

Encryption is when you change data with a special encoding process so that the data becomes unrecognizable (it's encrypted).  You can then apply a special decoding process and you will get the original data back. By keeping the decoding process a secret, nobody else can recover the original data from the encrypted data.

[Video - How asymmetric encryption works](https://invidious.snopyta.org/E5FEqGYLL0o)

<br>
![mailvelope](en/mailvelope.svg?resize=150)
You can use Mailvelope, a browser add-on in Chrome, Edge and Firefox, that securely encrypts your emails with PGP using Disroot webmail. Check this [howto](https://mailvelope.com/en/help).