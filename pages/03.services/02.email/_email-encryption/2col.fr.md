---
title: 'Chiffrement des e-mails'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## Qu'est-ce que le chiffrement?

---

Le chiffrement est lorsque vous modifiez des données à l'aide d'un procédé de codage spécial de sorte que les données deviennent illisibles (c'est chiffré).  Vous pouvez ensuite appliquer un processus de décodage spécial et récupérer les données originales.  En gardant le processus de décodage secret, personne d'autre ne peut récupérer les données originales à partir des données chiffrées.

[Video - How asymmetric encryption works](https://www.youtube.com/watch?v=E5FEqGYLL0o)

<br>
![mailvelope](en/mailvelope.svg?resize=150)
Vous pouvez utiliser Mailvelope, un module complémentaire de navigateur dans Chrome, Edge et Firefox, qui chiffre vos e-mails de manière sécurisée avec PGP en utilisant le webmail de Disroot. Consultez ce [tutoriel](https://mailvelope.com/fr/help).