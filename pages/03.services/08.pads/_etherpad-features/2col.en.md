---
title: 'Etherpad Features'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Color highlighting

All the changes in the text are marked with authorship color assigned to each user in the pad, making it easy to find who edited what.

## Text styling

Bold, italic, paragraphs, lists, bullet point lists, etc.

## Chat

Online chat for all users working on the text.

## History

Review the changes throught out the time with a time slider.

---

![](en/etherpad-example.png?lightbox=1024)
