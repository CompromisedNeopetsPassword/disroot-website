---
title: 'Caratteristiche di Git'
wider_column: left
---

## Facile da usare
Controlla i repository pubblici, trova quello su cui vuoi lavorare e clonalo sul tuo computer. Oppure creane uno tuo! Puoi impostarlo come un archivio pubblico o privato.

## Veloce e leggero
**Forgejo** offre un'esperienza molto veloce e leggera. Nessun set di funzionalità gonfio e troppo ingegnerizzato. Solo cose essenziali per collaborare al tuo progetto open source!

## Notifiche
Ricevi notifiche via posta elettronica quando viene risolto un problema o è stata creata una richiesta pull.

## Sicuro
Usa ssh, l'autentificazione a due fattori o GnuPG per proteggere il tuo repository.

---

![](en/forgejo_explore.png?lightbox=1024)

![](en/forgejo_secure.png?lightbox=1024)
