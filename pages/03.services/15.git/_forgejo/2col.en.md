---
title: Git
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Sign up</a>
<a class="button button1" href="https://git.disroot.org/">Log in</a>
<a class="button button1" href="http://kgtz2pmmov5jfvn3z4mqryffjnnw6krzrgxxoyaqhqckjrr4pckyhsqd.onion">Tor</a>

---

![forgejo_logo](forgejo.png?resize=100,100)


## Forgejo

**Disroot's Git** is powered by **Forgejo**. **Forgejo** is a community driven, powerful, easy to use and lightweight solution to code hosting and project collaboration. It's build around GIT technology which is the most widely used modern version control system in the world today.

Disroot Git: [https://git.disroot.org](https://git.disroot.org)

Project homepage: [https://forgejo.org/](https://forgejo.org/)

Source code: [https://codeberg.org/forgejo/forgejo](https://codeberg.org/forgejo/forgejo)

<hr>

You need to create a seperate account on [git.disroot.org](https://git.disroot.org/) to use this service. **Disroot** account credentials are not supported.
