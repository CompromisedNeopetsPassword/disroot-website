---
title: 'Seguro'
wider_column: left
---

# Seguro y protegido
<ul class=disc>
<li>La información es comprimida y cifrada en el navegador antes de ser enviada al servidor, utilizando [AES](https://es.wikipedia.org/wiki/Advanced_Encryption_Standard) de 256 bits. El servidor tiene cero conocimiento de la información almacenada.</li>
<li>Tu información está a salvo incluso en caso de violación o incautación del servidor.</li>
<li>Los motores de búsqueda están ciegos en relación al contenido pegado.</li>
<li>Las discusiones son cifradas/descifradas también en el navegador.</li>
<li>El servidor no puede ver comentarios o apodos.</li>
<li>Con la caducidad de pegado, puedes tener breves discusiones ad hoc que desaparecerán en el vacío luego de expirar. Esto no dejará rastros de tu discusión en tu casilla de correo.</li>
<li>Las discusiones no pueden ser indexadas por los motores de búsqueda.</li>
</ul>
---

![](en/privatebin02.gif)
