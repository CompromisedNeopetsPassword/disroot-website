---
title: Bin
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://bin.disroot.org/">Pastebin</a>
<a class="button button1" href="http://n63ite5off46lfh7qei4uhkvttrgvpve7ag3kwftlqkxo4o5mu7l4cqd.onion">Tor</a>

---


![](private-bin.png)
**PrivateBin** ist ein minimalistischer, quelloffener Online-Container mit zugehörigem Diskussionsboard. Die Daten jedes Pastebin werden im Browser Ende-zu-Ende-verschlüsselt, so dass der Server keine Kenntnis über die gehosteten Daten hat.

Du benötigst keinen Disroot-Account, um diesen Service zu nutzen.

Disroot Pastebin: [https://bin.disroot.org](https://bin.disroot.org)

Projekt-Website: [https://privatebin.info/](https://privatebin.info/)

Quellcode: [https://github.com/PrivateBin/PrivateBin](https://github.com/PrivateBin/PrivateBin)
