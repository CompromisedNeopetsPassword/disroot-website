---
title: 'Einfach'
wider_column: right
---


![](de/privatebin01.gif)

---

# Einfache Benutzung
<ul class=disc>
<li>Füge Text ein, klicke auf "Senden", teile die URL.</li>
<li>Lege ein Verfallsdatum fest: 5 Minuten, 10 Minuten, 1 Stunde, 1 Tag, 1 Woche, 1 Monat, 1 Jahr oder nie.</li>
<li>"Zerstören nach dem Lesen"-Option: Der Paste wird nach dem Lesen gelöscht.</li>
<li>Für jeden Paste wird eine einmalige Wegwerf-URL generiert.</li>
<li>Syntax-Coloration für 54 Sprachen (per highlights.js), gemischte Syntax wird unterstützt (html/css/javascript).</li>
<li>Automatisches Umwandeln von URL in klickbare Links (http, https, ftp und magnet).</li>
<li>Ein-Button-Lösung zum Klonen eines bestehenden Paste.</li>
<li>Diskussionen: Du kannst für jeden Paste Diskussionen erlauben.</li>
</ul>
