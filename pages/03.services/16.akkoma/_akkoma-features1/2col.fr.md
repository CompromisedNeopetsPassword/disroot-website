---
title: 'Akkoma Features'
wider_column: left
---

## Fédéré
Vous pouvez échanger des messages avec des personnes sur n'importe quel serveur ActivityPub ou OStatus, comme GNU Social, Friendica, Hubzilla et Mastodon.

## Écrivez ce que vous avez en tête !
Avec Akkoma, vous n'êtes pas limité à 150 caractères ! Vous pouvez ajouter des liens, des images, des sondages, etc.

## Notifications
Recevez des notifications lorsque quelqu'un vous ajoute comme contact, lorsque vous êtes mentionné dans un article ou lorsque l'un de vos articles est partagé par quelqu'un.

## Chat
Envoyez des messages aux autres membres d'Akkoma Disroot grâce à un chat en temps réel.

---
<b>

![](en/Akkoma_home.png?lightbox=1024)
