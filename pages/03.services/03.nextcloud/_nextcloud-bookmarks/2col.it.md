---
title: 'Segnalibri'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Scegli il tuo client preferito'
clients:
    -
        title: Bookmarks
        logo: en/bookmarks_logo.png
        link: https://f-droid.org/en/packages/org.schabi.nxbookmarks/
        text:
        platforms: [fa-android]
---

## Segnalibri

Un gestore di segnalibri per **Nextcloud**. Salva le tue pagine preferite e sincronizzale tra i tuoi device.

---

![](en/Bookmarks.png?lightbox=1024)
