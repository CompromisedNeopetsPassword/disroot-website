---
title: 'Signets'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Choisissez votre client préféré'
clients:
    -
        title: Bookmarks
        logo: en/bookmarks_logo.png
        link: https://f-droid.org/en/packages/org.schabi.nxbookmarks/
        text:
        platforms: [fa-android]
---

## Signets

Un gestionnaire de signets pour **Nextcloud**. Sauvegardez vos pages préférées et synchronisez vos appareils.

---

![](en/Bookmarks.png?lightbox=1024)
