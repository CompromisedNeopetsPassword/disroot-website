---
title: 'Nextcloud Keep or Sweep'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-sweep.png?lightbox=1024)

---

## Keep or Sweep

This helps you remove clutter by reminding you to delete files that you may have forgotten about.
