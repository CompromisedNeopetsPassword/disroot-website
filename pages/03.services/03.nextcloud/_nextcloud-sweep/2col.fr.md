---
title: 'Nextcloud Keep or Sweep'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-sweep.png?lightbox=1024)

---

## Garder ou Nettoyer

Cela vous aide à éliminer le désordre en vous rappelant de supprimer les fichiers que vous avez peut-être oubliés.
