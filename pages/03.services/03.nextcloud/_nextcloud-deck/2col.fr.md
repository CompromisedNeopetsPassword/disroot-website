---
title: 'Deck'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Choisissez votre client préféré'
clients:
    -
        title: Deck
        logo: en/deck_logo.png
        link: https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-deck.png?lightbox=1024)

---

## Deck

Deck vous aide à organiser votre travail, que ce soit le vôtre ou celui de votre équipe Nextcloud. Vous pouvez ajouter des tâches, les catégoriser, les partager et les commenter.
