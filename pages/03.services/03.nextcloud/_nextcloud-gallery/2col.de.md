---
title: 'Nextcloud Galerien'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right

---

![](de/nextcloud-gallery.png?lightbox=1024)

---

## Galerien

Teile Deine Bildergalerien mit Freunden und Famile. Gewähre ihnen Zugang, um Bilder hochzuladen, anzusehen und herunterzuladen. Schick einen Link an die von Dir gewählten Personen und bestimme selbst, ob sie diese Bilder wiederum mit anderen teilen dürfen.
