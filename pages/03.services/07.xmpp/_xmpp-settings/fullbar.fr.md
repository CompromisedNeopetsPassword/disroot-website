---
title: 'Réglages XMPP'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Paramètres du serveur
### Nom d'utilisateur:<span style="color:#8EB726"> Votre nom d'utilisateur@disroot.org</span>
#### Paramètres avancés : server/port<span style="color:#8EB726"> disroot.org/5222 </span>

---

#### Taille limite pour l'upload de fichiers:<span style="color:#8EB726"> 512 Mo </span>
#### Les messages et fichiers archivés expirent après <span style="color:#8EB726"> 1 mois </span>
<br>
