---
title: 'Lufi How'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## How does it work?

Drag and drop files in the appropriate area or use the traditional way to browse and select files and the files will be chunked, encrypted and sent to the server. You will get two links per file: a download link, that you give to the people you want to share the file with and a deletion link, allowing you to delete the file when you want.
You can see the list of your files by clicking on the *"My files"* link at the top right of that page.

You don't need to register yourself to upload files.

---

![](en/lufi-drop.png?lightbox=1024)
