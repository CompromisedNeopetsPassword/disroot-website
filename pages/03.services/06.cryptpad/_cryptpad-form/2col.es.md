---
title: 'Formularios'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](form.png)

## Formularios cifrados de extremo-a-extremo
**Formularios** te ofrece la posibilidad de crear y compartir formularios completamente cifrados.


---
![](presentation.png)

## Presentaciones cifradas sobre la marcha
Crea presentaciones cifradas de extremo-a-extremo con un editor sencillo junto a tus amigxs o compañerxs de organización o trabajo. Tu presentación finalizada puede ser "reproducida" directamente desde **CryptPad**.
