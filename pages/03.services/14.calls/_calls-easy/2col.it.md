---
title: 'Easy to use'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Semplice da usare
Basta avviare una riunione e condividere il suo collegamento con gli altri membri.
Non hai nemmeno bisogno di un account per usarlo e può essere eseguito nel tuo browser, senza installare alcun software aggiuntivo sul tuo computer. 

---

![](easy.png)
