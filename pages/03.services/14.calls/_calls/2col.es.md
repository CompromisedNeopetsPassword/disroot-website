---
title: Llamadas
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://calls.disroot.org/">Iniciar una conferencia</a>
<a class="button button1" href="http://oubguq76ii5svwyplbheorayf2nmoxy7inyd43a24adq24sy7jahjvyd.onion">Tor</a>

---

![](jitsi_logo.png?resize=150,150)

El servicio de **Llamadas** de **Disroot** es un software de video-conferencia, desarrollado por **Jitsi-Meet**. Proporciona conferencias de audio y video de alta calidad con tantas personas como quieras. También te permite transmitir tu escritorio o solo algunas ventanas a otrxs participantes de la llamada.


Llamadas Disroot: [https://calls.disroot.org/](https://calls.disroot.org/)

Página del proyecto: [https://jitsi.org/jitsi-meet/](https://jitsi.org/jitsi-meet/)

Código fuente: [https://github.com/jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet)
