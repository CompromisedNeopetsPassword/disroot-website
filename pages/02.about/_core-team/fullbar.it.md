---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Disroot Team

**Antilopa**, **Muppeth**, **Fede**, **Meaz**, **Avg_Joe**.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz") ![avg_joe](avg_joe.png "Avg_Joe")

## Team di traduzione
La traduzione in italiano è opera di **@l3o**, francese, spagnolo e tedesco sono a cura del **core team**.
*Se sei interessato ad aiutarci con questo lavoro, contattaci [qui](https://git.disroot.org/Disroot/Disroot-Project/issues/148)*
