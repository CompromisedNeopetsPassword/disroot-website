---
title: Federation and Decentralization
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

<br>
<br>
![](about-organize.jpg)

---

<br>
# Federazione e decentralizzazione

La maggior parte dei servizi internet sono gestiti da un punto centralizzato di proprietà e gestiti da società. Questi memorizzano i dati privati dei loro utenti e li analizzano utilizzando algoritmi avanzati per creare profili accurati dei loro "utenti". Queste informazioni sono spesso utilizzate per sfruttare le persone per il bene degli inserzionisti. Le informazioni possono anche essere ottenute da istituzioni governative o da malintenzionati. I dati possono essere rimossi senza preavviso a causa di dubbi motivi o politiche regionali di censura.

Un servizio decentralizzato può risiedere su più macchine, di proprietà di persone, aziende o organizzazioni diverse. Con i protocolli di federazione queste istanze possono interagire e formare una rete di molti nodi (= server che ospitano servizi simili). Si potrebbe essere in grado di spegnere un nodo ma mai l'intera rete. In una tale configurazione la censura è praticamente impossibile.

Pensa a come usiamo la posta elettronica; puoi scegliere qualsiasi fornitore di servizi o creare il tuo e scambiare e-mail con persone che utilizzano un altro fornitore di servizi. La posta elettronica si basa su un protocollo decentralizzato e federato.

Questi due principi, insieme, fanno da sfondo ad un'enorme rete che dipende da un'infrastruttura piuttosto semplice e da costi relativamente bassi. Qualsiasi macchina può diventare un server e partecipare alla rete alla pari. Da un piccolo computer desktop a casa vostra a un server dedicato o rack con più server. Questo approccio offre la possibilità di creare la più grande rete globale di proprietà dell'utente - proprio come Internet doveva essere.

Non vogliamo che Disroot diventi un'entità centralizzata, ma piuttosto una parte di una comunità più ampia - un nodo su molti. Speriamo che altri siano ispirati a creare più progetti con intenzioni simili.
