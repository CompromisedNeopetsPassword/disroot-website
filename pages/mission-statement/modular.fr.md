---
title: 'Déclaration de mission'
bgcolor: '#1F5C60'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _ambition
            - _practical
            - _tldr
            - _empty-bar
body_classes: modular
header_image: surfers.png

translation_banner:
    set: true
    last_modified: Mars 2022
    language: French
---
