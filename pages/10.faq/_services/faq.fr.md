---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SERVICES"
section_number: "400"
faq:
    -
        question: "Quels services puis-je utiliser avec mon compte Disroot?"
        answer: "<p>Votre compte vous permet d'utiliser les services suivants avec le même nom d'utilisateur et mot de passe:</p>
        <ul class=disc>
          <li>Email</li>
          <li>Cloud</li>
          <li>Forum</li>
          <li>Chat</li>
          <li>Tableau de projet</li>
        </ul>
        <p>Le reste des services (<b>Pads, PasteBin, Upload, Search, Polls</b>) ne nécessite aucune inscription.</p>"
    -
        question: "Quelles utilisations puis-je faire de mon compte e-mail?"
        answer: "<p>Vous pouvez l'utiliser comme bon vous semble, sauf à des fins commerciales ou pour l'envoi de Spam. Pour avoir une meilleure idée de ce que vous pouvez et ne pouvez pas en faire, veuillez lire les paragraphes 10 et 11 de nos <a href='https://disroot.org/fr/tos' target='_blank'>Conditions d'utilisation</a></p>"
    -
        question: "Quelle est la taille de la boîte de réception et du cloud?"
        answer: "<p>La limite de taille de <b>stockage des emails</b> est <b>1Go</b> et la limite de <b>taille des pièces jointes</b> est de <b>50Mo</b>.</p>
        <p><b>Le stockage sur le cloud</b> est de <b>2Go</b>.</p>
        <p>Il est possible d'étendre la capacité de stockage de votre cloud et de votre messagerie. Consultez les options <a href='https://disroot.org/fr/forms/extra-storage-space/' target='_blank'>ici</a></p>"
    -
        question: "Puis-je voir l'état de certains services comme le temps de disponibilité, la maintenance planifiée, etc. ?"
        answer: "<p>Oui. Et il y a de multiples façons d'être à jour avec tous les problèmes, la maintenance et les informations générales sur la santé de la plate-forme.</p>
        <ul class=disc>
        <li>Visitez <a href='https://status.disroot.org' target='_blank'>https://status.disroot.org</a></li>
        <li>Suivez <b>disroot@nixnet.social</b> (le fediverse à nouveau...)</li>
        <li>Rejoignez <b>state@chat.disroot.org</b> (XMPP)</li>
        <li>Installez <b>DisApp</b>, l'application Android de <b>Disroot</b>qui vous permettra de recevoir des notifications en temps réel</li>
        </ul>"
---
