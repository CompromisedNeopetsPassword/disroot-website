---
title: Online-donation
bgcolor: '#fff'
wider_column: left
fontcolor: '#1e5a5e'
---

![](premium.png?class=reward) **Disroot** depende de las donaciones y apoyo de su comunidad y usuarixs de los servicios. Si quieres ayudar a que el proyecto avance y haya más espacio para potenciales nuevxs disrooters, por favor, utiliza cualquiera de los métodos habilitados para realizar una contribución económica. [Vinculación de dominios](/services/email#alias) es disponible para donantes habituales.

También puedes colaborar comprando [espacio de almacenamiento extra para el correo](/services/email#storage) y/o [espacio de almacenamiento extra para la nube](/services/nextcloud#storage).

---

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Donar utilizando Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Conviértete en Mecenas" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://disroot.org/cryptocurrency"><img alt="Cryptomoneda" src="donate/_donate/c_button.png" /></a>

</div>

<span style="color:#4e1e2d; font-size:1.1em; font-weight:bold;">Transferencia bancaria:</span>
<span style="color:#4e1e2d; font-size:1.1em;">
Stichting Disroot.org
IBAN NL19 TRIO 0338 7622 05
BIC TRIONL2U
</span>
