---
title: 'Disroot App'
bgcolor: '#1F5C60'
fontcolor: '#fff'
text_align: left
wider_column: left
---

<br>

## Scaricala da  [![F-droid](F-Droid.svg.png?resize=80,80&class=imgcenter)](https://f-droid.org/en/packages/org.disroot.disrootapp/) [F-droid](https://f-droid.org/en/packages/org.disroot.disrootapp/?class=lighter)

---

## Disroot app
Questa app è come il tuo coltellino svizzero per la piattaforma Disroot, realizzata dalla community per la community. Se non disponi di un account Disroot, puoi comunque utilizzare questa app per accedere a tutti i servizi Disroot che non richiedono un account, come ad esempio upload.
