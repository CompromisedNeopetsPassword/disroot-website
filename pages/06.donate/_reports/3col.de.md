---
title: 'Jährliche Berichte'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

# Jahresberichte:

**Jahresbericht 2022**
[Bericht 2022](/annual_reports/AnnualReport2022.pdf?target=_blank)

**Jahresbericht 2020**
[Bericht 2020](/annual_reports/AnnualReport2020.pdf?target=_blank)

**Jahresbericht 2019**
[Bericht 2019](/annual_reports/AnnualReport2019.pdf?target=_blank)

**Jahresbericht 2018**
[Bericht 2018](/annual_reports/AnnualReport2018.pdf?target=_blank)

**Aug 2016 - Aug 2017**
[Bericht 2016-2017](/annual_reports/2017.pdf?target=_blank)

**2015 - Aug 2016**
Kosten: €1569.76 / Spenden: €264.52

---



---
<br>

# Hardware-Spenden
Wenn Du irgendwelche Hardware hast, die für dieses Projekt genutzt werden kann (Speicher, Server, CPUs, Festplatten, Raid-/Netzwerk-Karten, Netzwerkausstattung etc.), melde Dich bitte bei uns. Jede Hardware ist hilfreich. Was wir nicht brauchen, können wir verkaufen. Wir freuen uns außerdem über Angebote von Server-Racks.
