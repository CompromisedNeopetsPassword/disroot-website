---
title: 'Annual reports'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

#Annual reports:

**Annual Report 2022**
[Report 2022](/annual_reports/AnnualReport2022.pdf?target=_blank)

**Annual Report 2020**
[Report 2020](/annual_reports/AnnualReport2020.pdf?target=_blank)

**Annual Report 2019**
[Report 2019](/annual_reports/AnnualReport2019.pdf?target=_blank)

**Annual Report 2018**
[Report 2018](/annual_reports/AnnualReport2018.pdf?target=_blank)

**Aug 2016 - Aug 2017**
[Report 2016-2017](/annual_reports/2017.pdf?target=_blank)

**2015 - Aug 2016**
Costs: €1569.76 / Donation: €264.52

---



---
<br>

# Hardware donations
If you have any hardware that could be used for the project (memory modules, servers, CPUs, harddrives, Raid/Network Cards, Network equipment, etc) please get in touch with us. Any piece of hardware is welcome. Things we can't use we can sell to cash out. We are also looking forward to any rack-space propositions.
