---
title: Donaciones-online
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---


# "Si no estás pagando por el producto, el producto eres tú."

---

#### Donaciones en línea:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Donar a través de Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Convertirse en Mecena" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc-es&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="/cryptocurrency"><img alt="Criptomoneda" src="donate/_donate/c_button.png" /></a>

</div>

#### Transferencia bancaria:
<span style="color:#8EB726; font-size:1.8em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>

Tarjetas de crédito: <br><span style ="color:#8EB726;"> Puede usar el botón azul de Paypal para donaciones con tarjetas de crédito, no se necesita una cuenta de Paypal. </span>

#### Donación de Hardware: <span style="color:#8EB726;"> Mira más abajo </span>
