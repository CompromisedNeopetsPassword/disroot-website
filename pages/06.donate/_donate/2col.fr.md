---
title: Donation en ligne
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---


# "Si vous ne payez pas pour le produit, vous êtes le produit."

---

#### Donation en ligne:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Donner en utilisant Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Devenir mécène" src="donate/_donate/p_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=fr&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="/cryptocurrency"><img alt="Crypto monnaie" src="donate/_donate/c_button.png" /></a>

</div>

#### Virement bancaire:
<span style="color:#8EB726; font-size:1.8em;"> Stichting Disroot.org <br>
IBAN: NL19 TRIO 0338 7622 05<br>
BIC: TRIONL2U
</span>

Cartes de crédit: <br><span style ="color:#8EB726;">
Vous pouvez utiliser le bouton bleu Paypal pour les dons par carte de crédit. Un compte Paypal n'est pas nécessaire. </span>

#### Don de matériel: <span style="color:#8EB726;"> Voir ci-dessous </span>
