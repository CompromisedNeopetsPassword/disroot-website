---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Nous sommes reconnaissants pour vos dons et votre soutien !

Avec votre aide, nous nous rapprochons de notre objectif de viabilité financière, et nous prouvons qu'un modèle d'économie sociale est possible.

Pour remercier ceux d'entre vous qui font un don d'au moins 10€, nous vous enverrons un **Pack d'autocollants Disroot**. N'oubliez pas de laisser, dans la référence de votre don, l'adresse postale à laquelle nous devons l'envoyer.

![](stickers.jpg?resize=50%)