---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Благодарим вас за пожертвования и поддержку!

С вашей помощью мы приближаемся к достижению нашей цели финансовой устойчивости и доказываем, что социальная модель экономики возможна.

To thanks those of you that donate at least 10€, we will send a **Disroot stickers pack**. Don't forget to leave, in the reference of your donation, the postal address we should send this to.

![](stickers.jpg?resize=50%)