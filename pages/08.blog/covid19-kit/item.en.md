---
title: 'Covid19-kit'
media_order: antivir_mask.jpg
published: true
date: '31-03-2020 22:30'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - covid19
body_classes: 'single single-post'

---

So, looks like the world went totally crazy the last weeks. Our cities feel more like ghost towns and we are all trying to avoid contact with others to stop the spread of the virus. While all this chaos is going on, we sadly start giving up our online liberties to the tech giants. Our kids are now forced into Google classroom, and we are forced to use tools like MS-Teams, Zooms, Slacks, Discord and all. It looks as if we all have sign off our virtual lives away in fear and panic. What's worse is that the same fate can soon happen to our *IRL* (in real life). We need to keep things in check. We are launching Covid19 Kit to help and provide services to those that seek alternatives and don't want to get caught trapped inside the walled gardens. We decided to throw in some extra services that do not require account creation into the mix. Hopefully we will continue to improve the new experimental services (Mumble and Cryptpad) as well as add new services as we go based on your feedback and requirements. You can find **the Covid19 Kit page** here: [https://disroot.org/covid19](https://disroot.org/covid19). Of course the registrations and all other services continue to work as usual.


Additionally we are starting an experimental endeavor where we want to help setup and maintain individual, custom made platforms for organizations and small business that are looking for better, more ethical, transparent and FLOSS based service provider. We are still setting up so if you are interested, please hit us up on [email](mailto:support@disroot.org).


While we are trying to do our best to facilitate all your needs, we all must not forget that we are not the only ones out there. The internet should remain decentralized and distributed so that the user base spreads out evenly across the network. Remember, we are just a little fish in the ocean. Have a look at [https://libreho.st](https://libreho.st) or [https://chatons.org](https://chatons.org) and look for a provider that best suits your needs.
