---
title: 'DisNews #4 - Nextcloud performance; Framadate e Ethercalc; domini personalizzati XMPP; Lacre.io; fe.disroot.org'
date: '29-12-2021'
media_order: 2022.jpg
taxonomy:
  category: news
  tag: [disroot, news, nextcloud, etherpad, framadate, cryptpad, lacre,]
body_classes: 'single single-post'

---

Ciao Disrooters!

Vorremmo augurare a tutti voi un felice anno nuovo! Non soffermiamoci su quanto sia diventato pazzo il mondo e di quanto cupo si preannuncia il futuro. Concentriamoci invece sulle cose belle della vita. Immagina di essere seduto in una vasca idromassaggio all'aperto (o in una piscina fredda se ora ti trovi nell'emisfero australe). Ti stai godendo la tua bevanda preferita mentre una fresca brezza ti sfiora il viso, apri gli occhi e vedi le persone che ti sono più vicine, festeggiare con te gli ultimi minuti dell'anno.  
Il tempo sembra fermarsi in quel momento e nessuna delle tue preoccupazioni, dei tuoi cattivi pensieri, sembra essere reale. Bevi l'ultimo sorso del 2021 e prima di aprire gli occhi sei già nel 2022. Un anno che attendi con ansia, l'anno in cui finalmente libererai quella brutta abitudine di cui hai sempre voluto disfarti. L'anno in cui finalmente realizzerai quel bel progetto che hai sempre immaginato, l'anno in cui le restrizioni del governo diventano un ricordo del passato e tutti ti sorridono per le strade e si salutano "buongiorno"...


**Questo è ciò che auguriamo a tutti voi! Rendiamo il 2022 di nuovo fantastico!**

Bene, rieccoci. Sebbene la nostra ultima newsletter non sia stata di molto tempo fa, ci sono alcune piccole cose che vorremmo evidenziare.


# Performance di Nextcloud

Per alcune settimane la nostra istanza Nextcloud è stata più lenta del normale. Abbiamo rilevato il problema e l'abbiamo risolto in modo che ora le cose dovrebbero tornare lentamente alla normalità. Allo stesso tempo, stiamo lavorando per migliorare le prestazioni del servizio cloud in modo che sia di nuovo piacevole lavorare con questo servizio. Ci stiamo preparando ad acquistare nuovi server per alimentare la nostra infrastruttura, il che sarà sicuramente un grande impulso, ma fino ad allora vogliamo assicurarci di spremere quanto più possibile l'hardware attuale.


# Futuro di Framadate/Ethercalc

Nell'ultimo post ti abbiamo informato delle nostre preoccupazioni riguardo a **Framadate** e **EtherCalc**. L'idea è quella di rimuovere questi servizi a favore di **CryptPad** che fornisce una soluzione simile con il vantaggio di implementare la crittografia end-to-end e di avere uno sviluppo molto attivo. La schiacciante maggioranza di voi sembrerebbe sostenere questa idea e quindi, entro la fine di marzo 2022, chiuderemo **[Ethercalc](https://calc.disroot.org)** e **[Framadate](https://poll.disroot.org)** e consiglieremo CryptPad come soluzione per fogli di calcolo e sondaggi collaborativi. E visto che ne stiamo parlando, vi invitiamo a controllare più da vicino tutti i servizi presenti in CryptPad. CryptPad è, a nostro avviso, un ottimo modo per collaborare con gli altri, che si tratti di un documento di testo, un foglio di calcolo o una presentazione. Inoltre, è un software crittografato end-to-end che mantiene privato il contenuto di tutti i tuoi file e nemmeno noi, gli amministratori, possiamo leggerli.


# Domini personalizzati e XMPP

Siamo rimasti felicemente sorpresi e grati per le risposta che ci avete dato. Le richieste per i domini personalizzati per e-mail continuano ad arrivare e siamo felici di ciò. Dopotutto, pensiamo che sia l'opzione migliore per mantenere la propria presenza su Internet senza doversi preoccupare del provider che si utilizza, poiché si può spostare dove meglio si crede senza doversi preoccupare di informare tutti i propri contatti del cambiamento.

Sebbene forniamo anche il collegamento al dominio della chat XMPP, non siamo ancora stati in grado di elaborarne nessuno, ma non vi preoccupate, contatteremo presto tutti coloro che lo hanno richiesto. Restate sintonizzati!


# Sguardo al futuro

## Lacre.io

Sebbene non siamo stati in grado di lavorare sul progetto di crittografia della posta quanto avremmo voluto, siamo determinati a finalizzarlo nel prossimo anno. @pfm ha fatto magie e ha lavorato per aggiornare e utilizzare il software, quindi siamo quasi pronti per sottoporlo ad alcuni alpha test chiusi molto presto. Non vediamo l'ora!


## Fe.disroot.org - Un nuovo seme nel fediverso.

Per chi ancora non lo sapesse, il Fediverso è una rete di servizi federati, che spaziano dai social network, file sharing, image board e così via. È una grande rete in cui nessuna singola entità possiede tutte le risorse e i dati dell'utente. È una rete decentralizzata che promuove la libertà e la cooperazione tra diverse entità per creare una rete autogestita veramente indipendente di fornitori di servizi etici.
Uno degli obiettivi principali di Disroot è fornire servizi federati e quindi entrare nel protocollo ActivityPub (la principale forza trainante dietro al Fediverso) è qualcosa che volevamo fare da un po'. È da diverso tempo che stiamo infatti testando molte possibili soluzioni software. Sebbene pensiamo che **Hubzilla** sia il più avanzato e incentrato sulla privacy, è anche molto impegnativo per noi mantenerlo e renderlo facile da usare. Abbiamo quindi deciso di adottare un approccio diverso.
Invece di utilizzare un software per fare tutte le cose, abbiamo deciso di esaminare diversi servizi federati per attività specifiche. E così abbiamo iniziato con una piattaforma di microblogging (come Mastodon o Twitter) e siamo passati a **Pleroma**. È ancora in una fase di test, ma vi invitiamo a testarlo. Se hai già familiarità con il Fediverso, vorremmo usare il tuo feedback per rilevare tutti i piccoli problemi e bug rilevati. Nel frattempo, stiamo preparando tutti i tutorial necessari e le informazioni sul sito Web per rendere il più semplice possibile l'onboarding dei Disrooters che sono nuovi al concetto di Fediverso. Se sei interessato a testare le cose, puoi accedere alla nostra istanza Pleroma con le credenziali di Disroot su: **https://fe.disroot.org**. Se desideri partecipare alle discussioni sulle future app del fediverso che desideriamo fornirti, puoi unirti alla nostra chat federata all'indirizzo xmpp:fedisroot@chat.disroot.org .


**Ancora una volta: buon anno nuovo e auguriamo a tutti voi un fantastico 2022!**

Un ringraziamento speciale a tutti i Disrooters che ci hanno aiutato segnalando problemi, donando, frequentando la nostra chat (disroot@chat.disroot.org) e tutti coloro che hanno inviato patch, traduzioni e tutorial! Siete tutti fantastici!
Disroot non esisterebbe senza l'instancabile lavoro di tutti gli sviluppatori FLOSS. Vorremmo ringraziarvi tutti per il vostro duro lavoro! Tutti coloro che sono coinvolti nella creazione del kernel Linux, delle utilità GNU, di Debian e di tutte le altre distribuzioni (anche Arch :P), tutti coloro che stanno dietro ai progetti che usiamo quotidianamente in Disroot: **Nginx, Debian, CryptPad, Framasoft, Mumble, Pleroma, Soapbox, Hubzilla, Etherpad, Ethercalc, Taiga, Nextcloud, Lufi, ConverseJS, Privatebin, Searx, Jitsi, Gitea, Prosody, Mattermost, Postfix, Dovecot, Amavis, Rainloop and many many more...** Siete degli eroi! Buon anno!
