---
title: 'DisNews #4 - Performance Nextcloud; Framadate et Ethercalc; Domaines personnalisés et XMPP; Lacre.io; fe.disroot.org'
date: '29-12-2021'
media_order: 2022.jpg
taxonomy:
  category: news
  tag: [disroot, news, nextcloud, etherpad, framadate, cryptpad, lacre,]
body_classes: 'single single-post'

---

Salut les Disrooters !


Nous vous souhaitons à tous une bonne et heureuse année ! Ne parlons pas de la folie qui règne dans le monde et de l'avenir sombre qui s'annonce. Concentrons-nous plutôt sur les bonnes choses de la vie. Imaginez-vous assis dans un jacuzzi (ou dans une piscine froide si vous vous trouvez dans l'hémisphère sud). Vous dégustez votre boisson préférée tandis qu'une brise fraîche vous effleure le visage, vous ouvrez les yeux et vous voyez les personnes les plus proches de vous, fêtant avec vous les dernières minutes de l'année. Le temps semble s'arrêter à ce moment-là et aucun de vos soucis, aucune de vos mauvaises pensées, ne semble être réel. Vous buvez votre dernière gorgée de 2021 et avant d'ouvrir les yeux, vous êtes en 2022. Une année que vous attendez avec impatience, l'année où vous vous débarrasserez enfin de cette mauvaise habitude dont vous avez toujours voulu vous débarrasser, l'année où vous réaliserez enfin ce projet cool que vous avez toujours imaginé, l'année où les restrictions gouvernementales feront partie du passé et où tout le monde vous sourira dans la rue et se saluera en disant "bonjour"...

**C'est ce que nous vous souhaitons à tous ! Faisons en sorte que 2022 soit à nouveau génial !**

Très bien. Bien que notre dernière newsletter ne date pas d'hier, il y a quelques petites choses que nous aimerions souligner.


# Problème de performances de Nextcloud

Depuis quelques semaines, Nextcloud est plus lent que la normale. Nous avons depuis détecté le problème et l'avons corrigé de sorte que maintenant les choses devraient revenir à la lenteur normale. Dans le même temps, nous travaillons à l'amélioration des performances du service cloud afin qu'il soit à nouveau plus agréable de travailler dessus. Nous nous préparons à acheter de nouveaux serveurs pour alimenter notre infrastructure, ce qui sera certainement un grand coup de pouce, mais d'ici là, nous voulons nous assurer que nous tirons le maximum du matériel actuel pour garantir que la performance du cloud soit aussi optimisée que possible.


# L'avenir de Framadate/Ethercalc

Dans le dernier post, nous vous avons informé de nos préoccupations concernant **Framadate** et **EtherCalc** qui ne sont pas activement développés depuis un certain temps et de l'idée de supprimer ces services en faveur de **CryptPad** qui fournit une solution similaire avec l'avantage d'être chiffrée de bout en bout et très activement développée. L'écrasante majorité d'entre vous soutient cette idée et donc, d'ici fin mars 2022, nous fermerons **[Ethercalc](https://calc.disroot.org)** et **[Framadate](https://poll.disroot.org)** et recommanderons CryptPad comme solution pour les feuilles de calcul et les sondages collaboratifs avec une dose supplémentaire de sécurité. Et puisque nous en parlons, nous aimerions vous inviter à vérifier de plus près tous les services intégrés à CryptPad. CryptPad est, à notre avis, un excellent moyen de collaborer avec d'autres personnes, qu'il s'agisse d'un document texte, d'une feuille de calcul, d'un tableau de projet ou d'une présentation. De plus, c'est un logiciel chiffré de bout en bout qui garde le contenu de tous vos fichiers privés et que même nous, les administrateurs, ne pouvons pas lire.


# Domaines personnalisés et XMPP

Nous sommes très surpris et reconnaissants des réponses de chacun d'entre vous. Les demandes de liens vers des domaines personnalisés pour les e-mails continuent d'affluer et nous sommes heureux que vous vous lanciez dans l'aventure. Après tout, nous pensons que c'est la meilleure option pour conserver votre adresse e-mail/présence sur Internet sans avoir à vous soucier du fournisseur que vous utilisez, car vous pouvez la changer quand vous voulez sans avoir à vous soucier d'informer tous vos contacts de ce changement.

Bien que nous fournissions également la liaison de domaine de chat XMPP, nous n'en avons pas encore traité (priorités), mais nous contacterons bientôt tous ceux d'entre vous qui en ont fait la demande, alors restez à l'écoute !


## Des aperçus de l'avenir

## Lacre.io

Bien que nous n'ayons pas pu travailler sur le projet de chiffrement du courrier autant que nous l'aurions voulu et que nous n'ayons commencé que récemment, nous voulons le finaliser dans l'année à venir. @pfm a fait de la magie et a travaillé sur la mise à jour et l'utilisation du logiciel. Nous sommes donc presque prêts à le soumettre à des tests alpha très, très bientôt. Nous avons vraiment hâte d'y être.


## Fe.disroot.org - Une nouvelle graine dans le fediverse.

Pour ceux qui ne le savent pas encore, le Fediverse est un réseau de services fédérés, allant des réseaux sociaux au partage de fichiers, en passant par les tableaux d'images, etc. C'est un grand réseau où aucune entité unique ne possède toutes les ressources et les données des utilisateurs. Il s'agit d'un réseau décentralisé qui encourage la liberté et la coopération entre différentes entités afin de créer un réseau autogéré véritablement indépendant de fournisseurs de services éthiques.

L'un des principaux objectifs de Disroot est de fournir des services fédérés et, par conséquent, entrer dans le protocole ActivityPub (principal moteur de la Fediverse) est quelque chose que nous voulions faire depuis un certain temps. Nous avons testé de nombreuses solutions logicielles possibles depuis un certain temps déjà. Même si nous pensons que **Hubzilla** est la solution la plus avancée et la plus respectueuse de la vie privée, il est également très difficile pour nous de la maintenir et de la rendre conviviale. Nous avons décidé d'adopter une approche différente. Au lieu d'utiliser un seul logiciel pour tout faire, nous avons décidé d'examiner différents services fédérés et d'en utiliser un certain nombre qui font une seule chose. Nous avons donc commencé par une plateforme de microblogging (comme Mastodon ou Twitter) et avons opté pour **Pleroma**. Elle est encore en phase de test, mais elle est suffisamment utilisable pour être testée. Si vous êtes déjà familiarisé avec le Fediverse, nous aurions vraiment besoin de vos commentaires pour corriger tous les petits problèmes et bugs avant de le promouvoir pour une utilisation régulière. En attendant, nous préparons tous les tutoriels nécessaires et les informations sur le site web pour faciliter l'intégration des Disrooters qui ne connaissent pas le concept de Fediverse. Si vous souhaitez tester les choses, vous pouvez vous connecter à notre instance Pleroma avec les informations d'identification de votre Disroot à l'adresse suivante : **https://fe.disroot.org**. Si vous souhaitez participer aux discussions sur les futures applications fediverse que nous voulons fournir, vous pouvez rejoindre notre chat fédéré à xmpp:fedisroot@chat.disroot.org .


**Encore une fois : bonne célébration du nouvel an et nous vous souhaitons à tous une très bonne année 2022**.

Un grand merci à tous les Disrooters qui nous ont aidés en signalant des problèmes, en faisant des dons, en participant à notre chat (disroot@chat.disroot.org), à ceux qui ont aidé les autres à résoudre des problèmes liés à Disroot, à tous ceux qui ont soumis des correctifs, des traductions et des tutoriels ! Vous êtes tous géniaux !

Disroot n'existerait pas non plus sans le travail inlassable de tous les développeurs FLOSS. Nous tenons à vous remercier tous pour votre travail acharné ! Tous ceux qui ont participé à la création du noyau Linux, des utilitaires GNU, de Debian et de toutes les autres distributions (également Arch :P), tous ceux qui sont derrière les projets que nous utilisons quotidiennement à Disroot : **Nginx, Debian, CryptPad, Framasoft, Mumble, Pleroma, Soapbox, Hubzilla, Etherpad, Ethercalc, Taiga, Nextcloud, Lufi, ConverseJS, Privatebin, Searx, Jitsi, Gitea, Prosody, Mattermost, Postfix, Dovecot, Amavis, Rainloop et bien d'autres encore...** Vous êtes les héros ! Bonne et heureuse année !
