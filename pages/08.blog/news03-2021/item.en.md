---
title: "DisNews #3 - Lacre progress; Custom domain linking; New dashboard; Muppeth's quitting; 3rd volunteer fee"
media_order: news_image.jpg
published: true
date: '20-11-2021 14:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - lacre
        - custom_domain
        - volunteer
        - dashboard
body_classes: 'single single-post'
---

Hi there Disrooters.

*You have not heard from us in a while. In fact this post has been drafted already in August but as always (just realized this happens way too often lately), life came in the way and things got slightly delayed before we realized we should perhaps send this out. So pretend you are reading it somewhere in August.*

We are proud to let you know that we have been offering our collection of services for 6 years now. On the 4th of August, **Disroot** had its birthday. We are very happy with what we have managed to do so far and are looking forward to another 6 years to come. So next time you are having a drink have one for **Disroot** too.

Starting Disnews with such celebrative tone, let's see what's new since quite some time has passed since our last post.

# Lacre Test environment

Although still slow pacing, **Lacre**, our mailbox encryption project has gotten some work done. We have managed to setup proper test environment which will allow us to easily rebuild and break the setup. We have done some initial tests and we are ready to push things forward. **pfm** has already worked on the code, noted few issues, and is ready to update the codebase to Python3. The current course of action is as follows: First, we want to introduce code tests. Then we will update the codebase on the backend to python3 and the frontend to php7/8 without changing any features. Once those are done, we want to upstream the changes. That will be a starting point for us to sail the **Lacre** ship on a slightly different course than the upstream project. Of course we will be upstreaming changes, however, from that point onward some of the new or removed features might not be accepted by upstream (for example removal of decryption feature), which means that it might be very likely **Lacre** will start living it's own life. We are definitely looking forward to the next weeks. This, although in its infancy, is a very exciting time for open source mailbox encryption.
To make the deployment of our test infrastructure easier, we have backed up initial Ansible role for [**Lacre** backend](https://git.disroot.org/Disroot-Ansible/gpg-lacre) and pushed general [mailserver role](https://git.disroot.org/Disroot-Ansible/mailserver).

# Custom domain linking

Oh yes! After a very very long time the **custom domain** feature is back! We decided to change the game a little. Custom domain linking does remain as a "reward" for donation. However, unlike previously, this time donation linking is done for single donation and it's a lifetime feature! Much profit! Additionally, we are linking domain not only to email but also XMPP chat (although this last feature is still experimental). This means you can also chat with others using your own domain name. Upon request this can also mean creating chat rooms under your own domain too.

Although we decided that you no longer have to donate on regular basis, we do urge you to consider donating at least the equivalent of 12 coffees from your favorite coffee shop a year. *Please also remember that a recurring donation allows us to keep the project afloat, pay the bills for servers and hardware and maybe eventually put food on our tables*.

You can request domain to be linked with our email and XMPP chat via this [**link**](https://disroot.org/en/forms/domain-linking-form).


# New dashboard and Searx theme

We decided to combine both the [**dashboard**](https://apps.disroot.org) and our [**Searx** instance](https://search.disroot.org) in one. Now instead of just bare-bone dashboard with very little functionality we have a proper full blown landing page you can use as a shortcut to all other **Disroot** services and as a place to search the web. We are very happy with its unified look which we are slowly implementing across all services. You can choose between **beetroot** (light theme) and **darkbeet** (dark theme). In the future we will introduce more features in the main page such as alerts on issues or scheduled maintenance. We are looking forward to improvements to the main page and are awaiting your feedback. We hope you like the change, we sure do.


# Muppeth is quitting

Finally we can use this subject as a "clickbait". **Muppeth has decided to, quit...**

...his dayjob.

> After almost six years of living pretty much two lives with no rest and very little sleep, I have decided I can no longer sustain this while remaining healthy and keeping my sanity in check. Quitting **Disroot** was not an option as it's the most important project in my life, so I had to make the decision and choose for more uncertain, risky but surely exciting option. I decided I will focus my efforts on **Disroot** and try to turn it (one way or another) into a project that puts food on my family's table. I believe projects like **Disroot**, where your active presence is required 24/7 just like any commercial platform should be capable to make enough money ethically, treating their users with respect to their data and privacy. I believe better, more sustainable and ethical platforms should not be excluded from earning their living without sacrificing their ideas, and I think this can be achievable by **Disroot** and its vibrant community. Who else if not us.

We are very happy for **Muppeth** who made this very hard decision and we support his and the entire team's efforts to find a way to make **Disroot** financially sustainable. It really does not take that much to make that dream a reality. If every disrooter would decide to donate the equivalent of just one coffee a month in support of the platform they are using daily, we would be able upgrade the hardware we run disroot on, donate to FLOSS (Free/Libre and Open Source Software) projects to further advance the software we all use daily and benefit from and pay all the team living wages. No need for venture capital, shady investments, profit driven features or data mining.

So, don't be shy and [**donate**](https://disroot.org/en/donate).


# 3rd volounteer fee

To prove that the point above is within reach we would like to proudly announce that **we can now pay a volunteer fee to one more core team member**. Volunteer fees, in accordance to Dutch regulation, could be paid by non-profit foundation to volunteers, where the fee does not exceed 1700€ a year. We decided we will pay a volunteer fee of 140€ a month if certain conditions are met:

 - After all costs, donation to FLOSS projects and setting 400€ aside, we need to have over 140€ for a period of at least three months before we start paying out *(safety net)*.
 - 140€ is paid to a core team member until three consecutive months margin of 140€ was not reached *(safety belt)*.

We would like to thank all of you who decide to contribute financially to **Disroot**. We are very grateful for having such community of Disrooters around the project ❤️


# Future of Framadate and Ethercalc
FLOSS software is ever evolving and we, as part of that movement, are affected by it. Software in general evolves, keeps changing and improving but also sometimes stops progressing or even goes extinct. We are facing one of the later examples. For quite some time **"Ethercalc"**, the software that powers our service at [**https://calc.disroot.org**](https://calc.disroot.org) has not been well maintained, if at all. Recently **Framadate** which powers [**http://poll.disroot.org**](https://poll.disroot.org) has joined that state. Seeing both projects are on life support, we began to wonder if it makes sense to keep providing them. It means neither **Framadate** nor **Ethercalc** will receive any new features and in time unpatched security issues may occur.

Additionally, a piece of software we really came to love, **CryptPad**, a complete end-to-end encrypted zero knowledge collaboration suite which powers our [**https://cryptpad.disroot.org**](https://cryptpad.disroot.org) instance, does include poll and spreadsheet features. **CryptPad** is actively developed and provides something which in view of recent changes in the legislation not only in EU but around the world, is getting so much more important, end-to-end encryption. Encryption type where data on the servers is stored in a way that not even admins with full access to the server hardware could decrypt. We know that both of those services have been with us since the inception of **Disroot** and have been used by many Disrooters. We would like to get to know your feedback on this issue and see what you think. You can use any form of contact with us listed on our [**website**](https://disroot.org/contact). Additionally we wanted to create two simple polls so we could get some *guesstimation* over your general opinions on the matter. Please check below links:

[**Framadate/EtherCalc vs CryptPad Poll**](https://cryptpad.disroot.org/form/#/2/form/view/t+aoDjPYZRjdT-wZQer3TdKJn8X3NKJhbDQHvc+yNKg/)

Once again thank you for sailing with us for last 6 years! It's been a pleasure for us to support you with the services and it's been an amazing adventure so far. We have not yet said the last word and we see the journey is just beginning!

Thank you for being with us, your support, feedback, friendship, tears and love. Thanks for considering to use that little island on the internet sea and sticking around! See you around!
