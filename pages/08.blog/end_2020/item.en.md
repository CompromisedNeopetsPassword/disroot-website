---
title: 'Ufff... finally End of 2020'
media_order: f2020.jpg'
published: true
date: '31-12-2020 11:15'
taxonomy:
    category:
        - news
    tag:
        - nextcloud
        - conversejs
        - xmpp
        - news
body_classes: 'single single-post'

---
*Happy Solstice*

And what a year it was! There is no point in emphasizing how this year sucked for all of us and how everything seems to be upside down around the world. Let's just focus on positive stuff and enjoy the last days of the year.

We would like to wish you all a great celebrations and an awesome 2021! We would like to send special thank you to all those supporting us this year in all forms. We send a big hug to each and every Disrooter out there!!! Have a great year!

Now since we have been a bit slacky this year updating you guys on what has changed, let's see some of the little changes in the last weeks.

## Nextcloud 20
We have recently updated our **Nextcloud** instance to the latest version. This one brings tons of cool new features. The most noticable would be the Dashboard. As you probably noticed, there is a new dashboard app showing you a feed of changes in your cloud. Whether it's calendar events, new files, deck cards or chats, you can view all of this in one glance. Additionally there are plugins allowing you to add external services such as mastodon, github, gitlab, and more. As the list of plugins will grow we will add more options of course.

Additional change, especially for those using **Nextcloud** as a collaborative tool, is the status message. Now, you can easily display to others whether you are online, busy or anything in between. Known from other chat platforms, status allows others to see what you are busy with and if you are online to be bothered or not.

For those who use **Nextcloud Talk** we have enabled bridging to other chat platforms. It's still experimental feature but it's good to test it in the battle field. Bridges allows you to connect multiple chat platforms in one, where a bot relays everything said in the chat room to all of the networks bridged, allowing split communities to participate together. And so, one **Nextcloud Talk** room can be connected to IRC, Matrix, Mattermost, XMPP and various other rooms. Try it out and let us know of any issues so we could relay that to the **Nextcloud** staff.

Apart from all the good stuff and improvements, you might have also noticed that the performance of the cloud has improved greatly. Especially after we had issue with it for few weeks. We are happy things are back to normal and we are enjoying the cloud even more as Disroot Core Team, about which you can read below.

## Using Nextcloud Deck for the Disroot Core Team planning
From the very start, we've been using the services on Disroot as our main go-to solutions. Apart from being the admins we are also Disrooters ourselves, "dog-fooding" our platform as much as possible. We think that this aproach helps understand the issues and requirements better and we enjoy using our own creation. For last years we've used Taiga for planning our work and managing issues but recently we have decided to switch to Deck. One reason for it was that we have decided to drop SCRUM methodology as it was draining our energy, especially since Disroot is not our main occupation (believe it or not) and we still need to maintain dayjobs, families and other projects. The constant race from one sprint to another for the last 5 years, became too exhausting, and we could not keep up with the tempo we've created ourself. We concluded we do not need backlog, sprints and all that anymore and it was time to make better use of another tool we offer (you know dog fooding your own platform is best), so we decided to switch for a while from **Taiga** to **Nextcloud Deck**. To make it transparent and enable community to participate, we have created and shared both the Main project board (read only for non Core Members) and Issue board (where anyone can submit issues, feature requests, proposals, etc.) to the Disroot Community Circle. Any disrooter can join the "Disroot Community Circle" from circle app on **Nextcloud** to participate or check what we are currently busy with.

Additionally we have created a chatroom on **Nextcloud Talk**, which is bridged with XMPP, IRC and Matrix rooms. This means no matter which platform you use for communication you can easily connect to Disroot Community Hangout. \o/

So, join in, get active and have fun in our community circle!

## Darkbeet ConverseJS theme
The long promised and postponed theme for **ConverseJS** (our XMPP webchat of choice) has finally landed. There is still tons of things to improve, but *@muppeth* is very happy we finally managed to push something. It's a good start and we hope to push the theme once it's polished upstream. If guys at converseJS like it, it will be included into upstream source code.

![](converse_darkbeet.png)

You can use it already at [webchat](https://webchat.disroot.org) and if you are interested in the latest progress and like to live on the edge use our [devchat](https://devchat.disroot.org)

## Howto's update
This year we also started the necessary updating of the Howtos. *@Fede* is fully committed to this, it is a task that requires constant dedication and in the process we are finding errors and possibilities for improvement. So far, most of the **Nextcloud** basics are covered and the User section is up to date, but there is still a lot left to do.

The main goal of the Howto project is to provide and make accessible as much information, in as many languages, as possible about the services and software we use. As you can imagine, this requires a constant amount of work and time.

So, if you want to collaborate in the development of the Howtos, improve and translate the existing ones or send us comments, suggestions or even write about your experiences using Disroot services and share them with the community, you just have to communicate with us through the [XMPP room](xmpp:howto@chat.disroot.org?join) or by [email](mailto:howto@disroot.org). Any help is very welcome. Remember that you can also see the progress of it on the **Deck board** accessible through the Cloud through the **Howto circle**.



**So once again everyone. Fuck 2020 and happy new year!**
