---
title: 'New perk: Extra storage on Cryptpad'
date: '23-09-2023 09:10'
media_order: cryptpad.jpg
taxonomy:
  category: news
  tag: [disroot, news, cryptpad, storage, perks]
body_classes: 'single single-post'

---

## New perk: Extra storage on Cryptpad

We have been hosting cryptpad for several years now, and we're really happy about it.

Recently, some disrooters contacted us to see if they could have extra storage on Cryptpad, like we do for mail and cloud. So first we extended the **default free storage from 250MB to 500MB**.

We've also decided to allow user to allocate the extra storage they pay for as they wish between cloud, mail and now also Cryptpad. For example, if a Disrooter donates for 10GB storage, this user could decide to have 6GB for cloud, 2GB for mail and 2GB for Cryptpad. These extra gigabytes will be added on top of the default storage space given on account sign-up (so in that previous example the user would have 2,5GB of extra storage on cryptpad).

Check our [perks page](https://disroot.org/perks) to see the extra storage prices.