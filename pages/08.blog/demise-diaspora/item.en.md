---
title: 'Demise of Diaspora'
media_order: tunnel.jpg
published: true
date: '19-01-2020 18:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - roadmap
body_classes: 'Demise of Diaspora'
---

For the past year our interest in **Diaspora** as a social network solution has been declining. We knew that at one point there will come the time to make the decision about the future of our pod on the federation map. The moment has come to face the inevitable and to announce that we will be phasing out **Diaspora** in favor of something we feel has bigger potential and suits our platform better and something we've been invested in the past year.

The performance of our **Diaspora pod** for the last months has been terrible. People constantly complain about the slow stream load times, and we personally were not satisfied with its performance even though we did what we could to improve it. We have also been attacked by the spam account creation which lead us to decide to temporary close registrations while fighting it off. However, we have never re-opened the registrations which sadly reflects our lack of motivation to keep the pod running.

Do we have anything against **Diaspora**? Not at all.

Of course the performance could be improved. **Diaspora** has been with us from the very beginning. It was **Diaspora** where we first posted a hello world as **Disroot** project, and in fact was the one and only advertisement of **Disroot** done by us. It was a great experience and great community. However, nothing stays in one place. The times are changing (*"ecosystem is moving"*:wink:  :wink: )and we feel like we need to move on. We do not want to get to the situation where provide multiple social networks and thus we need to choose for a single solution we can stand behind, focus on and support as much as we can. For past four years it was **Diaspora** but looking around, there are amazing projects that stroke our interest and we've become more involved with. The overwhelming feature set of **Hubzilla** in terms of not only interoperability with other protocols, but also features such as groups, profiles, chats, forums, articles and of course, the nomadic identity and the Zot protocol in general, made us look at its direction more and more. On the other hand the ActivityPub powered networks show the potential of new, fresh and massively occupied federated networks. We feel a need for change and we are want to put our efforts at something we can identify with. Obviously we are currently invested into Hubzilla as we believe it is a brilliant social network ahead of its time, providing set of features unseen anywhere else, and yet very underestimated. That said we do not want to make "impulsive" decision. This is why we cannot tell at this stage which exactly social network we will eventually choose. It's most likely going to be Hubzilla but it could as well be Friendica, Pleroma, Pixelfed or even mastodon. At this stage it is too early to tell. We know Hubzilla has it's shortcomings and so before we manage to address those, it stays unanswered.

What we can say is that officially we will be phasing out our current Diaspora pod. So what does it mean for current users? Not much actually. **Diaspora** will be operational until the last active user shuts the lights off. We are not **Google** to kick people out like with **G+**. We are however going to:

-   Remove the **Diaspora** page from our website,
-   Close registration
-   Start with purging inactive accounts without possibility to reactivate. We will automatically purge users inactive for longer than 4 months without any prior warning.
-   In the meantime we will be working on Diaspora's replacement.

We would like to say thank you to Diaspora devs for providing us with first ever federated social network, for all the years you've been with us during and prior the birth of Disroot. We are tremendously grateful for your efforts and your work. We will do our best to not leave "The Federation" protocol and still stay in touch with the Disapora network. For now though, Thanks for all the fish, and see you on the flip side.
