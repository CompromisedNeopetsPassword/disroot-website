---
title: Faircoin
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

![](hacker-faircoin.png)

---

# Faircoin
Invia i tuoi faircoin al nostro portamonete:
fdFr8kpBwNmEtYkT6Qb4XMULNZi7VgRovh
