---
title: Calls
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: left
---

# Jitsi Calls
### Audio/video conference calls.

Disroot's Calls service is a videoconferencing software, powered by **Jitsi-Meet**. It provides you high quality video and audio conferences, with as many partners as you want. It also allows to stream your desktop or only some windows to other participants in the call.

<a class="button button2" href="https://calls.disroot.org/">Start a conference</a>

---

<br><br>
![jitsi_screenshot](jitsi.png?lightbox=1024)

