---
title: Chat Overview
section_id: chats
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
services:
    -
        title: XMPP Chat  
        icon: xmpp.png
        link: 'https://webchat.disroot.org'
        text: "Decentralized instant messaging."
        button: 'https://howto.disroot.org/en/tutorials/chat'
        buttontext: "Learn more"
    -
        title: Nextcloud Talk
        icon: cloud.png
        link: 'https://cloud.disroot.org/apps/spreed/'
        text: "??"
        button: 'https://howto.disroot.org/en/tutorials/cloud/apps/talk/web'
        buttontext: "Learn more"
    -
        title: 'Calls'
        icon: calls.png
        link: 'https://calls.disroot.org'
        text: "A videoconferencing tool."
        button: 'https://disroot.org/services/calls'
        buttontext: "Learn more"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://mumble.disroot.org'
        text: "A low latency, high quality voice chat application."
        button: 'https://howto.disroot.org/en/tutorials/office/mumble'
        buttontext: "Learn more"
    -
        title: 'Delat chat'
        icon: email.png
        link: 'https://disroot.org/services/delta'
        text: "??"
        button: ''
        buttontext: "Learn more"
---
