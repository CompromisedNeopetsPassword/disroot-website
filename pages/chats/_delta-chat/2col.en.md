---
title: Deltachat
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: left
---

# Delta Chat

### Messaging over e-mail. 

Delta Chat is an end-to-end encrypted messaging app that works over e-mail. Message anyone with an e-mail addres even if they don't use Delta Chat.

To know more about visit Delta Chat's [website](https://delta.chat).

---

<br>
![deltachat_screenshot](deltachat_screen.png?lightbox=1024)

