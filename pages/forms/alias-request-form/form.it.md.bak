---
title: 'Modulo di richiesta alias'
header_image: introverts.jpg
form:
    name: Alias-request-form
    fields:
        -
            name: username
            label: 'Nome Utente'
            placeholder: 'Inserisci il tuo nome utente disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Secondo Alias (facoltativo)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Terzo Alias (facoltativo)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Quarto Alias (facoltativo)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Fifth Alias (facoltativo)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: false
        -
            name: contribution
            label: 'Contributo tramite'
            placeholder: seleziona
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Bonifico bancario'
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: amount
            label: Importo
            placeholder: 'in €, $, BTC etc.'
            type: text
            validate:
                pattern: '[0-9.,]*'
                required: true
        -
            name: frequency
            label: Frequenza
            type: radio
            default: monthly
            options:
                monthly: Mensile
                yearly: Annuale
            validate:
                required: true
        -
            name: reference
            label: Riferimento
            type: text
            placeholder: 'Nome del titolare del pagamento o altro riferimento di trasferimento'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Invia
        -
            type: reset
            value: Azzera
    process:
        -
            email:
                from: alias-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] La tua richiesta di alias'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Grazie per il vostro contributo a Disroot.org!</strong><br>Apprezziamo davvero la sua generosità.<br><br>Esamineremo la vostra richiesta di alias e vi risponderemo appena possibile.</strong><br><br><hr><br><strong>Ecco una sintesi della richiesta ricevuta:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Alias request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'La vostra richiesta di alias è stata inviata!'
        -
            display: grazie
---

<h1 class="form-title"> Email Alias Request </h1>
<p class="form-text"><strong>Gli alias sono disponibili per i nostri sostenitori abituali.</strong> Dai sostenitori abituali pensiamo a coloro che contribuiscono con l'equivalente di almeno una tazza di caffè al mese.
 <br><br>
Non è che stiamo promuovendo il caffè, al contrario - il caffè è il simbolo di <a href="http://thesourcefilm.com/">sfruttamento</a> e <a href="http://www.foodispower.org/coffee/">disuguaglianza</a> quindi abbiamo pensato che fosse un buon modo per permettere alle persone di misurare quanto possono dare.
Abbiamo trovato questa <a href="https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee">lista</a> dei prezzi delle tazzine di caffè in tutto il mondo, potrebbe non essere molto preciso, ma dà una buona indicazione delle diverse tariffe.
<br><br>
<strong>Per favore, prendetevi il tempo di considerare il vostro contributo.</strong> Se puoi 'comprarci' una tazza di caffè Rio De Janeiro al mese va bene, ma se puoi permetterti un Frappuccino di soia a doppio decaffeinato con extra e crema al mese, allora puoi davvero aiutarci a mantenere in funzione la piattaforma Disroot e assicurarti che sia disponibile gratuitamente per altre persone con meno mezzi.</p>
