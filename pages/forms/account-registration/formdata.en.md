---
title: 'Account registration'
cache_enable: false
process:
    twig: true
---

<h2> Account request </h2>

<br> **Thank you for registering an account at Disroot. We will review your request and get back to you. Waiting time is usually not longer than 48 hours, but sometimes due to private life obligations or other priorities this might take longer. If you don’t get an email from us in few days, please reach out to support@disroot.org**
<br>
<br>
**Kind regards, <br>the Disroot team.**
