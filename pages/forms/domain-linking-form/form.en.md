---
title: 'Domain Linking Form'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
           type: spacer
           title: 'User and domain details:'
           text: "Provide details about your account and domain you want to link to our servers."
           markdown: true

        -    
          name: username
          label: 'User Name'
          placeholder: 'Enter your Disroot user name'
          autofocus: 'on'
          autocomplete: 'on'
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: Domain
          label: 'Your domain'
          placeholder: example.com
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: catchall
          markdown: true
          type: checkbox
          label: '**Catchall mail** - is a feature for your custom domain. It enables you to recieve any and all emails addressed to your domain even if not addressed to your email address (anything@yourdomain.ltd).'
          validate:
            required: false
        -
          name: xmpp
          markdown: true
          type: checkbox
          label: '**Advanced:** Do you want to use xmpp chat with your domain? Make sure you are pointing A record to xmpp.disroot.org'
          validate:
            required: false
        -
          type: spacer
          title: 'Alias information'
          markdown: true
          text: "Using aliases allows you to create multiple identities for your Disroot account which can be used with your custom domain *(eg. john@example.com or nickname@example.com)*. Remember that those are still connected to your single account and you can have maximum of 5 per account. If you want to add additional user accounts to make use of your domain, you need to contact us via email or add additional accounts and aliases you want to add to them in description below. Any additional aliases or changes need to be requested by contacting support until automated solution is provided."

        -
          name: 'Alias 1'
          label: Alias
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: true
        -
          name: 'Alias 2'
          label: 'Second Alias (optional)'
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 3'
          label: 'Third Alias (optional)'
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 4'
          label: 'Forth Alias (optional)'
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          name: 'Alias 5'
          label: 'Fifth Alias (optional)'
          placeholder: your-alias
          type: text
          validate:
            pattern: '[A-Za-z0-9.-]*'
            required: false
        -
          type: spacer
          title: "Donation details"
          markdown: true
          text: "Please provide below details on the donation you have made. Please make sure you do it before you request the domain linking feature. **This information will be removed the moment your request is processed.**"

        -
          name: contribution
          label: 'Contributing via'
          placeholder: select
          type: select
          options:
            patreon: Patreon
            stripe: 'Stripe (creditcard)'
            paypal: Paypal
            bank: 'Bank transfer'
            faircoin: Faircoin
            bitcoin: Bitcoin
          validate:
            required: true

        -
          name: amount
          label: Amount
          placeholder: 'EUR/USD/BTC/etc.'
          type: text
          validate:
            pattern: '[A-Za-z0-9., ]*'
            required: true

        -
          name: comments
          type: textarea

        -
          name: honeypot
          type: honeypot
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Your domain linking request'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Thank you for your contribution to Disroot.org!</strong><br>We truly appreciate your generosity.<br><br>We will review your request and get back to you as soon as we can.</strong><br><br><hr><br><strong>Here is a summary of the received request:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Domain linking request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Your domain linking request has been sent!'
        -
            display: thankyou
---

<h1 class="form-title"> Domain Linking Request </h1>
<p class="form-text"><strong> Fill in this form if you would like to use your own domain for Email aliases and/or xmpp chat.</strong>
<br><br>
Make sure you followed all the steps needed before you request your domain. Care about your and our time.
We review all pending requests once every week so be patient. We will get back to you as soon as we process your request.  
</p>
<br><br><br><br>
