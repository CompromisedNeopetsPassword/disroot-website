---
title: 'Espacio de Almacenamiento Extra'
form:
    name: 'Espacio de Almacenamiento Extra'
    fields:
        -
            name: username
            label: 'Nombre de usuario'
            placeholder: 'Ingresa el nombre de usuario de Disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Dirección de correo de Contacto'
            placeholder: 'Ingresa una dirección de correo para contactarte'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Cantidad de Espacio'
            label: 'Espacio total de almacenamiento'
            type: select
            default: 5GB
            options:
                5GB: '5GB por 11€ anuales'
                10GB: '10GB por 20€ anuales'
                15GB: '15GB por 29€ anuales'
                30GB: '30GB por 56€ anuales'
                45GB: '45GB por 83€ anuales'
                60GB: '60GB por 110€ anuales'
            validate:
                required: true
        -
            name: 'Tamaño del Correo'
            label: '¿Cuántos de estos GB quieres para el correo?'
            placeholder: 'Ingresa el espacio deseado para el almacenamiento de correo'
            autofocus: 'on'
            autocomplete: 'on'
            type: number

        -
            name: 'Tamaño de la Nube'
            label: '¿Cuántos de estos GB quieres para la nube?'
            placeholder: 'Ingresa el espacio deseado para el almacenamiento en la nube'
            autofocus: 'on'
            autocomplete: 'on'
            type: number     
        -
            name: 'Cryptpad size'
            label: 'How many of these GB do you want for cryptpad?'
            placeholder: 'Enter the desired size for cryptpad storage'
            autofocus: 'on'
            autocomplete: 'on'
            type: number
        -
            name: cryptpadkey
            label: 'Cryptpad public key'
            placeholder: 'We need your cryptpad public key to add storage to it, please copy/paste it here'
            type: text           
        -
            name: comments
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Detalles del Pago:**'
        -
            name: country
            label: 'País (Necesario por razones de I.V.A)'
            placeholder: 'Por favor, especifica el país en el que está registrada tu cuenta de pago'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Pago a través de:'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Transferencia bancaria (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Enviar
        -
            type: reset
            value: Reiniciar
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Solicitud de Almacenamiento Extra'
                body: 'Estimad@ {{ form.value.username }}, <br><br> Hemos recibido una solicitud de espacio extra de almacenamiento. <br><br>Deberías recibir tu confirmación por correo con tu referencia de facturación. <br> Una vez que hayamos recibido tu pago, te adjudicaremos el espacio adicional. <br><br> ¡Gracias por apoyar a Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: '¡Tu solicitud ha sido enviada!'
        -
            display: thankyou
---

<h1 class="form-title"> Espacio de almacenamiento extra </h1>
<p class="form-text">Con tu cuenta de <b>Disroot</b>, obtienes almacenamiento GRATUITO: 1GB para tus correos, 2GB para la nube. Sin embargo, ofrecemos la posibilidad de ampliarlo.<br>
Los precios son <b>por año</b> y <b>las tasas de pagos están incluidas</b>.<br>
Sin embargo, <b>lxs usuarixs Europexs</b> tienen un <b>I.V.A</b> (Impuesto al Valor Agregado) que es un 21% más.<br>
