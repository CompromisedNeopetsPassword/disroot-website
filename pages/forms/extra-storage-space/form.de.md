---
title: 'Zusätzlicher Speicherplatz'
form:
    name: 'Extra Storage Space'
    fields:
        -
            name: username
            label: 'Benutzername'
            placeholder: 'Gib Deinen Disroot-Benutzernamen an'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Kontakt-Email-Addresse'
            placeholder: 'Gib Deine Kontakt-Email-Addresse an'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Amount of Space'
            label: 'Gesamt-Speicherplatz'
            type: select
            default: 5GB
            options:
                5GB: '5GB für 11€ pro Jahr'
                10GB: '10GB für 20€ pro Jahr'
                15GB: '15GB für 29€ pro Jahr'
                30GB: '30GB für 56€ pro Jahr'
                45GB: '45GB für 83€ pro Jahr'
                60GB: '60GB für 110€ pro Jahr'
            validate:
                required: true
        -
            name: 'Mail size'
            label: 'Wie viele GB vom zusätzlichen Speicherplatz möchtest du für E-Mail nutzen?'
            placeholder: 'in Gigabyte eingeben'
            autofocus: 'on'
            autocomplete: 'on'
            type: number

        -
            name: 'Cloud size'
            label: 'Wie viele GB vom zusätzlichen Speicherplatz möchtest du für Cloud nutzen?'
            placeholder: 'in Gigabyte eingeben'
            autofocus: 'on'
            autocomplete: 'on'
            type: number
        -
            name: 'Cryptpad size'
            label: 'Wie viele GB vom zusätzlichen Speicherplatz möchtest du für Cryptpad nutzen?'
            placeholder: 'in Gigabyte eingeben'
            autofocus: 'on'
            autocomplete: 'on'
            type: number
        -
            name: cryptpadkey
            label: 'Cryptpad public key'
            placeholder: 'Wir benötigen deinen public key von Cryptpad um Speicherplatz hinzuzufügen. Bitte hier einfügen.'
            type: text
        -
            name: comments
            label: Anmerkungen
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Zahlungsmodalitäten:**'
        -
            name: country
            label: 'Land (notwendig wegen VAT)'
            placeholder: 'Bitte geben Sie das Land an, in dem Ihr Bankkonto oder Zahlungsdienst registriert ist'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Bezahlung per'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: 'Banküberweisung (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Absenden
        -
            type: reset
            value: Zurücksetzen
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Anfrage zusätzlicher Speicher'
                body: 'Hi {{ form.value.username }}, <br><br>wir haben Deine Anfrage auf zusätzlichen Speicherplatz erhalten. <br><br>Du solltest eine Bestätigung per Email erhalten, die auch die Zahlungsanweisungen enthält. <br>Wenn wir Deine Zahlung erhalten haben, werden wir Dir den zusätzlichen Speicherplatz zuweisen. <br><br>Vielen Dank für Deine Unterstützung von Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'Deine Anfrage wurde gesendet!'
        -
            display: thankyou
---

<h1 class="form-title"> Zusätzlicher Speicherplatz </h1>
<p class="form-text">Mit deinem <b>Disroot</b>-Account erhältst Du FREIEN Speicherplatz: 1GB für E&#8209;Mails 2GB Cloudspeicherplatz. Es ist möglich diesen Speicherplatz zu erweitern.<br>
Preise sind <b>pro Jahr</b> und <b>Zahlungsgebühren inklusive</b>.<br>
Für<b>Europäische Nutzer</b> wird jedoch zusätzlich eine <b>VAT</b> (Value Added Tax) fällig, die 21% beträgt.<br>
