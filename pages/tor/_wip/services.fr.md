---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: "SMTP/IMAP/POP3"
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email'
        text: "Comptes de messagerie gratuits et sécurisés pour votre client IMAP de bureau ou via une interface Web."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/566'
        buttontext: "Git issue"
    -
        title: Cloud
        icon: cloud.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/nextcloud/'
        text: "Vos données sous votre contrôle ! Collaborez, synchronisez et partagez des fichiers, des calendriers, etc."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/569'
        buttontext: "Git issue"
    -
        title: Chat XMPP
        icon: xmpp.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/xmpp'
        text: "Messagerie instantanée décentralisée."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/567'
        buttontext: "Git issue"
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/cryptpad'
        text: "Une alternative privée aux outils de bureautique populaires."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/570'
        buttontext: "Git issue"
    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/akkoma'
        text: "Un outil de microblogging qui peut fédérer avec d'autres serveurs qui supportent ActivityPub."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/571'
        buttontext: "Git issue"
---
