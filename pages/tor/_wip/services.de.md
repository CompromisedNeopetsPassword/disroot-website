---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: "SMTP/IMAP/POP3"
        icon: email.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/email'
        text: "Freie und sichere E&#8209;Mail-Accounts, nutzbar mit einem IMAP-Client oder via Online-Benutzeroberfläche."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/566'
        buttontext: "Git issue"
    -
        title: Cloud
        icon: cloud.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/nextcloud/'
        text: "Deine Daten unter Deiner Kontrolle! Sichern, Synchronisieren, Teilen von Kalendern, Kontakten und mehr."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/569'
        buttontext: "Git issue"
    -
        title: XMPP-Chat
        icon: xmpp.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/xmpp'
        text: "Dezentralisiertes, sicheres und freies Instant-Messaging."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/567'
        buttontext: "Git issue"
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/cryptpad'
        text: "Eine schon vom Design her vertraulich gestaltete Alternative zu gängigen Office-Tools."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/570'
        buttontext: "Git issue"
    -
        title: 'Akkoma'
        icon: akkoma.png
        link: 'http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion/services/akkoma'
        text: "A microblogging tool that can federate with other servers that support ActivityPub."
        button: 'https://git.disroot.org/Disroot/Disroot-Project/issues/571'
        buttontext: "Git issue"
---
