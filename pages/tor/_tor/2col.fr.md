---
title: Tor
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://www.torproject.org/download/" target="_blank">Télécharger Tor Browser</a>

---

![](Tor_Logo.png)


## Accéder au site Web de Disroot par Tor

Vous pouvez accéder au site Web de **Disroot** par Tor en utilisant le navigateur Tor. Tor empêche les personnes qui surveillent votre connexion de savoir que vous visitez le site Web de Disroot. 

Il suffit de copier et de coller cette adresse .onion dans Tor Browser : **j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion** ou si vous utilisez déjà le navigateur Tor, cliquez [ici](http://j4dhkkxfcsvzvh3p5djkmuehhgd6t6l7wmzih6b4ss744hegwkiae7ad.onion)

Page d'accueil du projet: [https://www.torproject.org/](https://www.torproject.org/)
