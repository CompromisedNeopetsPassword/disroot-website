---
title: Tor
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _tor
            - _green_bar
            - _torrified
            - _blue_bar
            - _wip
body_classes: modular
header_image: etienne-girardet-CxTCcjUo2hM-unsplash.jpg

translation_banner:
    set: true
    last_modified: Juillet 2023
    language: French
---
