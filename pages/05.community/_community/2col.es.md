---
title: 'Mapa de ruta'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

# Comunidad

---      

La **transparencia** juega un papel muy importante aquí en **Disroot**.
Queremos ser abiertos respecto a nuestros planes, ideas y progreso actual.

También queremos fomentar las contribuciones al proyecto desde la comunidad de **Disroot**. Por lo tanto, hemos montado un conjunto de herramientas para que sea sencillo para todxs seguir el desarrollo de **Disroot**, y también ser parte activa de las discusiones, reportar problemas y enviar ideas y arreglos.
